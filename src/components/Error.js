import React from 'react';
import {Link} from 'react-router-dom';

/*react-bootstrap components*/
import {Container} from 'react-bootstrap'


export default function ErrorPage(){

	return(
		<Container className="m-4 text-center">
			<h1>404 - Page Not Found!</h1>
			<p>The requested URL/ badpage was not found on this server. </p>
			<Link as={Link} to="/">Return to Home Page</Link>   
		</Container>
	)
}